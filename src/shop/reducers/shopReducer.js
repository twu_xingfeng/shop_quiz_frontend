import {ADD_PRODUCT, ADD_TO_CART, DELETE_CART, GET_ORDERS, GET_PRODUCTS} from "../utils/apiClient";

const initState = {
    carted:true,
    products: [],
    orders:[]
};

const shopReducer = (state = initState, action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload.products
            };

        case ADD_PRODUCT:
            return {
                ...state,
                created: action.payload.created
            };

        case ADD_TO_CART:
            return {
                ...state,
                carted: action.payload.carted
            };

        case GET_ORDERS:
            return {
                ...state,
                orders: action.payload.orders
            };

        case DELETE_CART:
            return {
                ...state,
                deleted: action.payload.deleted
            };
        default:
            return state
    }
};

export default shopReducer;
