import * as React from "react";

import './InputWithLabel.less'

class InputWithLabel extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <section className={"input-with-label"}>
                <section className={"input"}>
                    <p><label><span className={"star"}>*</span>{this.props.label}</label></p>
                    <input onChange={this.props.onChange} placeholder={this.props.placeHolder}/>
                </section>
            </section>
        );
    }

}

export default InputWithLabel;