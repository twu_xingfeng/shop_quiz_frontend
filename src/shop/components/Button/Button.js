import * as React from "react";

import './Button.less'

class Button extends React.Component{
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <button disabled={this.props.disable} className={"button"} onClick={this.props.onClick}>{this.props.name}</button>
        );
    }

}

export default Button;