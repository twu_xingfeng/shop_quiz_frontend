import * as React from "react";

import './AddButton.less'

class AddButton extends React.Component{
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <button disabled={this.props.disabled} className={"add-button"} onClick={this.props.onClick}>{this.props.name}</button>
        );
    }

}

export default AddButton;