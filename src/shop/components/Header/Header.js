import * as React from "react";
import {NavLink} from "react-router-dom";

import './Header.less'
import {IoIosAdd, IoIosCart, IoIosHome} from "react-icons/io";

class Header extends React.Component{
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <section className={"header"}>
                <ul>
                    <li><NavLink exact className={"header_nav"} activeClassName={"header_nav_active"} to={"/"}><IoIosHome/> 商城</NavLink></li>
                    <li><NavLink className={"header_nav"} activeClassName={"header_nav_active"} to={"/order"}><IoIosCart/> 订单</NavLink></li>
                    <li><NavLink className={"header_nav"} activeClassName={"header_nav_active"} to={"/add"}><IoIosAdd/> 添加商品</NavLink></li>
                </ul>
            </section>
        );
    }

}

export default Header;