const URL_HOST = "http://localhost:8080/api/";

export const GET_PRODUCTS = "GET_PRODUCTS";
export const ADD_PRODUCT = "ADD_PRODUCT";
export const ADD_TO_CART = "ADD_TO_CART";
export const GET_ORDERS = "GET_ORDERS";
export const DELETE_CART = "DELETE_CART";

export const getProductsFromApi = (dispatch) => {
    fetch(URL_HOST+"products").then(response => response.json().then(data => {
        console.log(data);
        dispatch({
            type: GET_PRODUCTS,
            payload: {
                products: data
            }
        })
    }));
};

export const addProductFromApi = (data, dispatch) => {
    fetch(URL_HOST+"products", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    }).then(response => {
        console.log(response);
        getProductsFromApi(dispatch);
        if(response.status===400){
            alert("商品名称已经存在，请重新输入商品名称");
        }
        dispatch({
            type: ADD_PRODUCT,
            payload: {
                created: true
            }
        })
    });
};


export const getOrdersFromApi = (dispatch) => {
    fetch(URL_HOST+"cart").then(response => response.json().then(data => {
        console.log(data);
        dispatch({
            type: GET_ORDERS,
            payload: {
                orders: data
            }
        })
    }));
};

export const addToCartFromApi = (id, dispatch) => {
    dispatch({
        type: ADD_TO_CART,
        payload: {
            carted: false
        }
    });
    fetch(URL_HOST+"products/"+id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    }).then(response => {
        console.log(response);
        dispatch({
            type: ADD_TO_CART,
            payload: {
                carted: true
            }
        })
    });
};


export const deleteOrderFromApi = (id, dispatch) => {
    fetch(URL_HOST+"cart/"+id, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    }).then(response => {
        console.log(response);
        getOrdersFromApi(dispatch);
        dispatch({
            type: DELETE_CART,
            payload: {
                deleted: true
            }
        })
    });
};