import * as React from "react";
import InputWithLabel from "../../components/InputWithLabel/InputWithLabel";

import './AddProduct.less'
import Button from "../../components/Button/Button";
import {addProduct, getProducts} from "../../actions/shopActions";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";

class AddProduct extends React.Component {
    constructor(props) {
        super(props);

        this.product = {
            name: "",
            price: "",
            unit: "",
            imgUrl: ""
        };

        this.state = {
            disabled:true
        };

        this.checkDisable = this.checkDisable.bind(this);
    }

    nameChange(e) {
        this.product.name = e.currentTarget.value;
        this.checkDisable();
    }

    priceChange(e) {
        this.product.price = e.currentTarget.value;
        this.checkDisable();
    }

    unitChange(e) {
        this.product.unit = e.currentTarget.value;
        this.checkDisable();
    }

    imgChange(e) {
        this.product.imgUrl = e.currentTarget.value;
        this.checkDisable();
    }

    checkDisable(){
        if (this.product.name === "" || this.product.price === "" || this.product.unit === "" || this.product.imgUrl === "") {
            this.setState({
                disabled:true
            })
        }
        if (this.product.name !== "" && this.product.price !== "" && this.product.unit !== "" && this.product.imgUrl !== "") {
            this.setState({
                disabled:false
            })
        }
    }

    addProduct() {
        this.props.addProduct(this.product);
    }


    render() {
        return (<section className={"add-container"}>
            <h3>添加商品</h3>
            <InputWithLabel label={"名称"} placeHolder={"名称"} onChange={this.nameChange.bind(this)}/>
            <InputWithLabel label={"价格"} placeHolder={"价格"} onChange={this.priceChange.bind(this)}/>
            <InputWithLabel label={"单位"} placeHolder={"单位"} onChange={this.unitChange.bind(this)}/>
            <InputWithLabel label={"图片"} placeHolder={"url"} onChange={this.imgChange.bind(this)}/>
            <Button disable={this.state.disabled?"disabled":""} onClick={this.addProduct.bind(this)} name={"提交"}/>
        </section>);
    }
}


const mapStateToProps = state => ({
    created: state.shopReducer.created
});

const mapDispatchToProps = dispatch => bindActionCreators({
    addProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct);