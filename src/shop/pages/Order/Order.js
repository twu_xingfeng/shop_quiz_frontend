import * as React from "react";
import {deleteOrder, getOrders} from "../../actions/shopActions";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";

import './Order.less'
import {NavLink} from "react-router-dom";

class Order extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getOrders();
    }

    deleteCart(id) {
        this.props.deleteOrder(id);
    }

    render() {
        return (
            <section className={"cart"}>
                {this.props.orders.length === 0 ? <p>暂无订单,<NavLink to={"/"}>返回商城</NavLink>页面继续购买</p>:
                    <section className={"table"}>
                        <section className={"th"}>
                            <section className={"td"}>名字</section>
                            <section className={"td"}>单价</section>
                            <section className={"td"}>数量</section>
                            <section className={"td"}>单位</section>
                            <section className={"td"}>操作</section>
                        </section>
                        {
                            this.props.orders.map(order => {
                                return (<section key={order.id} className={"tr"}>
                                    <section className={"td"}>{order.name}</section>
                                    <section className={"td"}>{order.price}</section>
                                    <section className={"td"}>{order.number}</section>
                                    <section className={"td"}>{order.unit}</section>
                                    <section className={"td"}>
                                        <button onClick={this.deleteCart.bind(this, order.id)}>删除</button>
                                    </section>
                                </section>);
                            })
                        }
                    </section>
                }
            </section>);
    }
}


const mapStateToProps = state => ({
    orders: state.shopReducer.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getOrders,
    deleteOrder
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);