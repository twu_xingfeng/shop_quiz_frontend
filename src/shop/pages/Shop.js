import * as React from "react";
import {BrowserRouter, Link} from 'react-router-dom';
import {Route} from "react-router";

import './Shop.less';
import Home from "./Home/Home";
import Order from "./Order/Order";
import AddProduct from "./AddProduct/AddProduct";
import Header from "../components/Header/Header";

export default class Shop extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className={"shop"}>
                <BrowserRouter>
                    <Header/>
                    <section className={"shop-container"}>
                        <section className={"container"}>
                            <Route exact path="/" component={Home}/>
                            <Route path="/order" component={Order}/>
                            <Route path="/add" component={AddProduct}/>
                        </section>
                        <p className={"footer-copy"}>TW Mail @2018 </p>
                    </section>
                </BrowserRouter>
            </section>
        );
    }

}