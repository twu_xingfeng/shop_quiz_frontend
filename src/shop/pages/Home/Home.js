import * as React from "react";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {addToCart, getProducts} from "../../actions/shopActions";

import './Home.less';
import AddButton from "../../components/AddButton/AddButton";
import {IoIosAdd} from "react-icons/io";

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getProducts();
    }

    addToCart(id) {
        this.props.addToCart(id);
    }

    render() {
        return (
            <section className={"products"}>{
                this.props.products.map(product => {
                    return (<section key={product.id} className={"product-container"}>
                        <section className={"product"}>
                            <img alt={product.name} className={"product-img"} src={product.imgUrl}/>
                            <p>{product.name}</p>
                            <p>单价{product.price}{product.unit}</p>
                            <AddButton disabled={this.props.carted ? "" : "disabled"} name={<IoIosAdd/>}
                                       onClick={this.addToCart.bind(this, product.id)}/>
                        </section>
                    </section>);
                })
            }
            </section>
        );
    }
}


const mapStateToProps = state => ({
    products: state.shopReducer.products,
    carted: state.shopReducer.carted
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts,
    addToCart
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);