import {
    addProductFromApi,
    addToCartFromApi,
    deleteOrderFromApi,
    getOrdersFromApi,
    getProductsFromApi
} from "../utils/apiClient";

export const getProducts  = () => (dispatch)=>{
    getProductsFromApi(dispatch);
};

export const addProduct  = (data) => (dispatch)=>{
    addProductFromApi(data, dispatch);
};

export const addToCart = (id) => (dispatch)=>{
    addToCartFromApi(id, dispatch);
};


export const getOrders  = () => (dispatch)=>{
    getOrdersFromApi(dispatch);
};


export const deleteOrder = (id) => (dispatch)=>{
    deleteOrderFromApi(id, dispatch);
};