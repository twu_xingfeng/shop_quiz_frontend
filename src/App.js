import React, {Component} from 'react';
import './App.less';
import Shop from "./shop/pages/Shop";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Shop/>
      </div>
    );
  }
}

export default App;