import {combineReducers} from "redux";
import shopReducer from "../shop/reducers/shopReducer";

const reducers = combineReducers({
    shopReducer: shopReducer
});
export default reducers;